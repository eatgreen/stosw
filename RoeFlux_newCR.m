% Roe flux ============================================================
function F = RoeFlux_newCR(hl,hr,ul,ur,vl,vr,flag)

flagX=flag;
flagY=~flag;

F=zeros(size(hl,1),size(hl,2),3);
%initial parameter
g=9.806;

%YY,F=zeros(size(hl,1),size(hl,2),3);
%initial parameter
%YY,g=9.806;

% Roe averages ------------------
duml=hl.^0.5;
dumr=hr.^0.5;

hhat=duml.*dumr;
uhat=(duml.*ul + dumr.*ur)./(duml+dumr);
vhat=(duml.*vl + dumr.*vr)./(duml+dumr);

chat=((0.5*g*(hl+hr)).^0.5);
chatl=((g*hl).^0.5);
chatr=((g*hr).^0.5);

dh=hr-hl;
du=ur-ul;
dv=vr-vl;

uNl = ul.*flagX + vl.*flagY;
uNr = ur.*flagX + vr.*flagY;

% Eigenvalues --------------------
al1=uNl-chatl;
al3=uNl+chatl;
ar1=uNr-chatr;
ar3=uNr+chatr;

a1 = abs(uhat*flagX + vhat*flagY - chat);
a2 = abs(uhat*flagX + vhat*flagY) ;
a3 = abs(uhat*flagX + vhat*flagY + chat);
 
% Entropy correction?
da1=max(0,2*(ar1-al1));
da3=max(0,2*(ar3-al3));

tmpIndex=a1<da1;
a1(tmpIndex)=0.5*(a1(tmpIndex).*a1(tmpIndex)./da1(tmpIndex)+da1(tmpIndex));

tmpIndex2=a3<da3;
a3(tmpIndex2)=0.5*(a3(tmpIndex2).*a3(tmpIndex2)./da3(tmpIndex2)+da3(tmpIndex2));

% Right eigenvectors ----------------
R21=uhat-flagX*chat;
R23=uhat+flagX*chat;
R31=vhat-flagY*chat;
R33=vhat+flagY*chat;

% Wave coefficients x eigenvalues
alpha1 = 0.5 * (dh - hhat.*du*flagX - hhat.*dv*flagY)./chat.*a1;
alpha2 = 0.5 * (hhat.*dv*flagX - hhat.*du*flagY).*a2;
alpha3 = 0.5 * (dh + hhat.*du*flagX + hhat.*dv*flagY)./chat.*a3;

% alpha1 = 0.5 * (dh - (hhat.*du*flagX + hhat.*dv*flagY)./chat).*a1;
% alpha2 = 0.5 * (hhat.*dv*flagX - hhat.*du*flagY).*a2;
% alpha3 = 0.5 * (dh + (hhat.*du*flagX + hhat.*dv*flagY)./chat).*a3;

% Left flux
FL1=uNl.*hl;
FL2=ul.*uNl.*hl + 0.5*g*hl.*hl*flagX;
FL3=vl.*uNl.*hl + 0.5*g*hl.*hl*flagY;

% Right flux
FR1=uNr.*hr;
FR2=ur.*uNr.*hr + 0.5*g.*hr.*hr*flagX;
FR3=vr.*uNr.*hr + 0.5*g.*hr.*hr*flagY;

% Roe flux
F(:,:,1)=0.5*(FL1+FR1-alpha1-alpha3);
F(:,:,2)=0.5*(FL2+FR2-(R21.*alpha1)-(alpha2.*(-flagY))-(R23.*alpha3));
F(:,:,3)=0.5*(FL3+FR3-(R31.*alpha1)-(alpha2.*(flagX))-(R33.*alpha3));
end