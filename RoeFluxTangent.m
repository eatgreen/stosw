% Roe flux ============================================================
function F = RoeFluxTangent(hl,hr,ul,ur,vl,vr,h0l,h0r,u0l,u0r,v0l,v0r,flag)

flagX=flag;
flagY=~flag;

F=zeros(size(hl,1),size(hl,2),3);
%initial parameter
g=9.806;

% Roe averages ------------------
duml=zeros(size(hl)); dumr=zeros(size(hl));
tmpIndex=find(h0l>0);
duml(tmpIndex) = 0.5*hl(tmpIndex)./(h0l(tmpIndex).^0.5);
dum0l=h0l.^0.5; 
tmpIndex=find(h0r>0);
dumr(tmpIndex) = 0.5*hr(tmpIndex)./(h0r(tmpIndex).^0.5);
dum0r=h0r.^0.5; 

hhat=duml.*dum0r + dum0l.*dumr;
hhat0=dum0l.*dum0r;
uhat=((duml.*u0l+dum0l.*ul+dumr.*u0r+dum0r.*ur).*(dum0l+dum0r) ... 
        - (dum0l.*u0l+dum0r.*u0r).*(duml+dumr))./((dum0l+dum0r).^2);
uhat0=(dum0l.*u0l + dum0r.*u0r)./(dum0l+dum0r);
vhat=((duml.*v0l+dum0l.*vl+dumr.*v0r+dum0r.*vr).*(dum0l+dum0r) ... 
        - (dum0l.*v0l+dum0r.*v0r).*(duml+dumr))./((dum0l+dum0r).^2);
vhat0=(dum0l.*v0l + dum0r.*v0r)./(dum0l+dum0r);

chat=zeros(size(hl)); chatl=zeros(size(hl)); chatr=zeros(size(hl));

tmpIndex=find((0.5*g*(h0l+h0r))>0);
chat(tmpIndex)=0.25*g*(hl(tmpIndex)+hr(tmpIndex))./((0.5*g*(h0l(tmpIndex)+h0r(tmpIndex))).^(0.5)); 
chat0=((0.5*g*(h0l+h0r)).^0.5);
tmpIndex=find((g*h0l)>0);
chatl(tmpIndex)=0.5*g*hl(tmpIndex)./((g*h0l(tmpIndex)).^(0.5)); 
chat0l=((g*h0l).^0.5);  
tmpIndex=find((g*h0r)>0);
chatr(tmpIndex)=0.5*g*hr(tmpIndex)./((g*h0r(tmpIndex)).^(0.5)); 
chat0r=((g*h0r).^0.5);  

dh=hr-hl;
du=ur-ul;
dv=vr-vl;
dh0=h0r-h0l;
du0=u0r-u0l;
dv0=v0r-v0l;

uNl = ul.*flagX + vl.*flagY;
uNr = ur.*flagX + vr.*flagY;
uN0l = u0l.*flagX + v0l.*flagY;
uN0r = u0r.*flagX + v0r.*flagY;

% Eigenvalues --------------------
al1=uNl-chatl;
al3=uNl+chatl;
ar1=uNr-chatr;
ar3=uNr+chatr;
a0l1=uN0l-chat0l;
a0l3=uN0l+chat0l;
a0r1=uN0r-chat0r;
a0r3=uN0r+chat0r;

tmpIndex = find((uhat0*flagX + vhat0*flagY - chat0)<0);
a1= uhat*flagX + vhat*flagY - chat; 
a1(tmpIndex) = -a1(tmpIndex); 
a01=abs(uhat0*flagX + vhat0*flagY - chat0);

tmpIndex = find((uhat0*flagX + vhat0*flagY)<0);
a2= uhat*flagX + vhat*flagY; 
a2(tmpIndex) = -a2(tmpIndex); 
a02=abs(uhat0*flagX + vhat0*flagY);

tmpIndex = find((uhat0*flagX + vhat0*flagY + chat0)<0);
a3= uhat*flagX + vhat*flagY + chat; 
a3(tmpIndex) = -a3(tmpIndex); 
a03=abs(uhat0*flagX + vhat0*flagY + chat0);

% Entropy correction?
da1=zeros(size(hl)); 
tmpIndex=find(2*(a0r1-a0l1)>0);
da1(tmpIndex)=2*(ar1(tmpIndex)-al1(tmpIndex));
da01=max(2*(a0r1-a0l1),0);
da3=zeros(size(hl));
tmpIndex=find(2*(a0r3-a0l3)>0);
da3(tmpIndex)=2*(ar3(tmpIndex)-al3(tmpIndex));
da03=max(2*(a0r3-a0l3),0);

% F(:,:,1)=a1;
% F(:,:,2)=a2;
% F(:,:,3)=a3;

tmpIndex=find(a01<da01);
a1(tmpIndex)=0.5.*((2*a1(tmpIndex).*a01(tmpIndex).*da01(tmpIndex)...
                -a01(tmpIndex).^2.*da1(tmpIndex))./da01(tmpIndex).^2+da1(tmpIndex));
a01(tmpIndex) = 0.5.*(a01(tmpIndex).*a01(tmpIndex)./da01(tmpIndex)+da01(tmpIndex));
tmpIndex=find(a03<da03);
a3(tmpIndex)=0.5.*((2*a3(tmpIndex).*a03(tmpIndex).*da03(tmpIndex)...
                -a03(tmpIndex).^2.*da3(tmpIndex))./da03(tmpIndex).^2+da3(tmpIndex));
a03(tmpIndex) = 0.5.*(a03(tmpIndex).*a03(tmpIndex)./da03(tmpIndex)+da03(tmpIndex));

% Right eigenvectors ----------------
R21=uhat-flagX*chat; R021=uhat0-flagX*chat0;
R23=uhat+flagX*chat; R023=uhat0+flagX*chat0;
R31=vhat-flagY*chat; R031=vhat0-flagY*chat0;
R33=vhat+flagY*chat; R033=vhat0+flagY*chat0;

% Wave coefficients x eigenvalues
alpha1 = (0.5.*(dh-(hhat.*du0+hhat0.*du).*flagX-(hhat.*dv0+hhat0.*dv).*flagY).*chat0...
    - 0.5.*(dh0-hhat0.*du0.*flagX-hhat0.*dv0.*flagY).*chat).*a01./chat0.^2 ...
    + 0.5.*(dh0-hhat0.*du0.*flagX-hhat0.*dv0.*flagY).*a1./chat0;
alpha01 = 0.5 *(dh0 - hhat0.*du0*flagX - hhat0.*dv0*flagY)./chat0.*a01;
alpha2 = 0.5.*((flagX.*(hhat.*dv0+hhat0.*dv)-flagY.*(hhat.*du0+hhat0.*du)).*a02 ...
    +(hhat0.*dv0.*flagX-hhat0.*du0.*flagY).*a2);
% alpha02 = 0.5 * (hhat0.*dv0*flagX - hhat0.*du0*flagY).*a02;
alpha3 = (0.5.*(dh+(hhat.*du0+hhat0.*du).*flagX+(hhat.*dv0+hhat0.*dv).*flagY).*chat0...
    - 0.5.*(dh0+hhat0.*du0.*flagX+hhat0.*dv0.*flagY).*chat).*a03./chat0.^2 ...
    + 0.5.*(dh0+hhat0.*du0.*flagX+hhat0.*dv0.*flagY).*a3./chat0;
alpha03 = 0.5.*(dh0+hhat0.*du0.*flagX+hhat0.*dv0.*flagY)./chat0.*a03;

% Left flux
FL1=uN0l.*hl + uNl.*h0l;
FL2=(ul.*uN0l+u0l.*uNl).*h0l + u0l.*uN0l.*hl + g*h0l.*hl*flagX;
FL3=(vl.*uN0l+v0l.*uNl).*h0l + v0l.*uN0l.*hl + g*h0l.*hl.*flagY;

% Right flux
FR1=uN0r.*hr + uNr.*h0r;
FR2=(ur.*uN0r+u0r.*uNr).*h0r + u0r.*uN0r.*hr + g*h0r.*hr*flagX;
FR3=(vr.*uN0r+v0r.*uNr).*h0r + v0r.*uN0r.*hr + g*h0r.*hr.*flagY;

% Roe flux
F(:,:,1)=0.5*(FL1+FR1-alpha1-alpha3);
F(:,:,2)=0.5*(FL2+FR2-(R21.*alpha01+R021.*alpha1)+alpha2.*flagY-(R23.*alpha03+R023.*alpha3));
F(:,:,3)=0.5*(FL3+FR3-(R31.*alpha01+R031.*alpha1)-alpha2.*flagX-(R33.*alpha03+R033.*alpha3));

end

