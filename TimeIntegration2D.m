% Time integration

% function Y = TimeIntegration2D(X,dt,dx,dy,itabs,itsigma,itsampleinterval,option)
function Y = TimeIntegration2D(X,dt,dx,dy,option,sigmaxx,sigmayy)
option.dt=dt;

if nargin==7
option.sigmaxx=sigmaxx;
option.sigmayy=sigmayy;
else
option.sigmaxx=0;
option.sigmayy=0;
end

switch option.orderT
    
    case 1 % 1st order Euler time integration -----------------------------
        
        %         DataStudy(Advection(X,option,dx,dy),'Tangent l1')
        Y = X + dt*Advection(X,option,dx,dy);
        
    case 3 % 3rd order Runge Kutta integration ----------------------------
        switch option.dynstep
            case 'dyn'
                option.X0=[];
                k1=Advection(X,option,dx,dy);
                k2=Advection(X+0.5*dt*k1,option,dx,dy);
                k3=Advection(X-dt*k1+2*dt*k2,option,dx,dy);
                
                Y = X + dt*((1/6)*k1+(2/3)*k2+(1/6)*k3);
                
            case 'tan'
                optionDyn=option;
                optionDyn.X0=[];
                optionDyn.step='dyn';
                
                X0=option.X0;
                k1=dt*Advection(X0,optionDyn,dx,dy);
                k2=dt*Advection(X0+0.5*k1,optionDyn,dx,dy);
                
                option.X0=X0;
                l1=dt*Advection(X,option,dx,dy);
                option.X0=X0+0.5*k1;    
                l2=dt*Advection(X+0.5*l1,option,dx,dy);
                option.X0=X0-k1+2*k2;      
                l3=dt*Advection(X-l1+2*l2,option,dx,dy);
                
                Y = X+(1/6)*l1+(2/3)*l2+(1/6)*l3;
                
            case 'adj'
                optionDyn=option;
                optionDyn.X0=[];
                optionDyn.step='dyn';
                
                X0=option.X0;
                k1=dt*Advection(X0,optionDyn,dx,dy);
                k2=dt*Advection(X0+0.5*k1,optionDyn,dx,dy);
                
                option.X0=X0-k1+2*k2;
                l1=dt*Advection(X,option,dx,dy);
                
                option.X0=X0+0.5*k1;         
                l2=dt*Advection(X+0.5*l1,option,dx,dy);
                
                option.X0=X0;
                l3=dt*Advection(X-l1+2*l2,option,dx,dy);
                
                Y = X+(1/6)*l1+(2/3)*l2+(1/6)*l3;
                                
        end
        
    case 4 % 4th order Runge Kutta integration ----------------------------
        switch option.step
            case 'dyn'
                k1=Advection(X,option,dx,dy);
                k2=Advection(X+0.5*k1,option,dx,dy);
                k3=Advection(X+0.5*k2,option,dx,dy);
                k4=Advection(X+k3,option,dx,dy);
                
                Y = X + (1/6)*k1+(1/3)*k2+(1/3)*k3+(1/6)*k4;
                
            case {'tan','adj'}
                optionDyn=option;
                optionDyn.X0=[];
                optionDyn.step='dyn';
                
                X0=option.X0;
                
                k1=Advection(X0,optionDyn,dx,dy);
                k2=Advection(X0+0.5*k1,optionDyn,dx,dy);
                k3=Advection(X0+0.5*k2,optionDyn,dx,dy);
                
                if (strcmp(option.step,'tan'))
                    
                    l1=Advection(X,option,dx,dy);
                    option.X0=X0+0.5*k1;
                    l2=Advection(X+0.5*l1,option,dx,dy);
                    option.X0=X0+0.5*k2;
                    l3=Advection(X+0.5*l2,option,dx,dy);
                    option.X0=X0+k3;
                    l4=Advection(X+0.5*l2,option,dx,dy);
                    
                    Y = X + (1/6)*l1+(1/3)*l2+(1/3)*l3+(1/6)*l4;
                    
                else
                    
                    option.X0=X0+k3;
                    l1=Advection(X,option,dx,dy);
                    option.X0=X0+0.5*k2;
                    l2=Advection(X+0.5*l1,option,dx,dy);
                    option.X0=X0+0.5*k1;
                    l3=Advection(X+0.5*l2,option,dx,dy);
                    option.X0=X0;
                    l4=Advection(X+l3,option,dx,dy);
                    
                    Y = X + (1/6)*l1+(1/3)*l2+(1/3)*l3+(1/6)*l4;
                    
                end
                
        end
        
end


end