% Roe flux ============================================================
function F = RoeFlux_newX(hl,hr,ul,ur,vl,vr,locunc_term,viscosity_term,topol,topor)

locunc_flag=locunc_term.flag;
whitenoise_flag=locunc_term.whitenoise;
viscosity_flag=viscosity_term.flag;

F=zeros(size(hl,1),size(hl,2),3);
%initial parameter
g=9.806;

% Roe averages ------------------
duml=hl.^0.5;
dumr=hr.^0.5;

hhat=duml.*dumr;
uhat=(duml.*ul + dumr.*ur)./(duml+dumr);
vhat=(duml.*vl + dumr.*vr)./(duml+dumr);

chat=((0.5*g*(hl+hr)).^0.5);
chatl=((g*hl).^0.5);
chatr=((g*hr).^0.5);

dh=hr-hl;
du=ur-ul;
dv=vr-vl;

uNl = ul;
uNr = ur;

% Eigenvalues --------------------
al2=uNl+chatl;
al3=uNl-chatl;
ar2=uNr+chatr;
ar3=uNr-chatr;

a1 = abs(uhat) ;
a2 = abs(uhat + chat);
a3 = abs(uhat - chat);

% Entropy correction?
da2=max(0,2*(ar2-al2));
da3=max(0,2*(ar3-al3));

tmpIndex=a2<da2;
a2(tmpIndex)=0.5*(a2(tmpIndex).*a2(tmpIndex)./da2(tmpIndex)+da2(tmpIndex));

tmpIndex2=a3<da3;
a3(tmpIndex2)=0.5*(a3(tmpIndex2).*a3(tmpIndex2)./da3(tmpIndex2)+da3(tmpIndex2));

% Right eigenvectors ----------------
R21=0;
R22=uhat+chat;
R23=uhat-chat;
R31=1;
R32=vhat;
R33=vhat;

% Wave coefficients x eigenvalues
% alpha1 = (hhat.*dv*flagX - hhat.*du*flagY).*a1;
% alpha2 = 0.5 * (dh + hhat.*du*flagX + hhat.*dv*flagY)./chat.*a2;
% alpha3 = 0.5 * (dh - hhat.*du*flagX - hhat.*dv*flagY)./chat.*a3;

alpha1 = -(hhat.*dv+dh.*vhat).*a1;
alpha2 = 0.5 * (dh + (hhat.*du)./chat).*a2;
alpha3 = 0.5 * (dh - (hhat.*du)./chat).*a3;

if strcmp(locunc_flag,'Off')
    
if strcmp(viscosity_flag,'Off')
% Left flux
FL1=uNl.*hl;
FL2=ul.*uNl.*hl + 0.5*g.*(hl.*hl-topol.*topol);
FL3=vl.*uNl.*hl;

% Right flux
FR1=uNr.*hr;
FR2=ur.*uNr.*hr + 0.5*g.*(hr.*hr-topor.*topor);
FR3=vr.*uNr.*hr;

else
dx=viscosity_term.dx;
dy=viscosity_term.dy;
% miu=viscosity_term.miu;

[Gxul]=gradient(ul,dx);
% [Gxvl,Gyvl]=gradient(vl,dx,dy);
[Gxur]=gradient(ur,dx);
% [Gxvr,Gyvr]=gradient(vr,dx,dy);

% miu=dx*dy*sqrt(Gxul.^2+Gyvl.^2+0.5*(Gyul+Gxvl).^2);
miu=100*dx*dy*sqrt(Gxul.^2);

% Left flux
FL1=uNl.*hl;
FL2=ul.*uNl.*hl + 0.5*g.*(hl.*hl-topol.*topol) - (miu.*hl.*Gxul);% + miu.*hl.*Gyul);
FL3=vl.*uNl.*hl + 0.5*g.*(hl.*hl-topol.*topol);% - (miu.*hl.*Gxvl + miu.*hl.*Gyvl);

% a1=ul.*uNl.*hl;
% b1=0.5*g*(hl.*hl-topol.*topol);
% c1=-miu.*hl.*Gxul;
% d1=a1+b1+c1;
% plot(a1(1,:))
% hold
% plot(b1(1,:),'r');
% plot(c1(1,:),'k');
% plot(d1(1,:),'g');

% Right flux
FR1=uNr.*hr;
FR2=ur.*uNr.*hr + 0.5*g.*(hr.*hr-topor.*topor) - (miu.*hr.*Gxur);% + miu.*hr.*Gyur);
FR3=vr.*uNr.*hr + 0.5*g.*(hr.*hr-topor.*topor);% - (miu.*hr.*Gxvr + miu.*hr.*Gyvr);

end

else
dt=locunc_term.dt;
dx=locunc_term.dx;

sigmaxx=locunc_term.sigmaxx;

sigmaxx=[sigmaxx sigmaxx(:,end) ;sigmaxx(end,:) sigmaxx(end,end)];

Gxhl=gradient(hl,dx);
Gxul=gradient(ul,dx);
Gxvl=gradient(vl,dx);
Gxhr=gradient(hr,dx);
Gxur=gradient(ur,dx);
Gxvr=gradient(vr,dx);

% Construct the diffusion operator
% sigmaxx=1-Ghx.*Ghx/(norm([Ghx,Ghx])^2);
% sigmaxy=sigmaxx-1;
% sigmayy=sigmaxx;
 
axx=sigmaxx.^2;

% axx=2*dx*sqrt(Gxul.^2+0.5*(Gxvl).^2);
Gxaxx=gradient(axx,dx);

% Standard brownian motion process
if strcmp(whitenoise_flag,'On')
dBtx=sqrt(dt)*randn;
sigmadBtx=sigmaxx*dBtx;

% Left flux
FL1=uNl.*hl - 0.5*(axx.*Gxhl + 2*Gxaxx.*hl) + (hl.*sigmadBtx)/dt;
FL2=ul.*uNl.*hl + 0.5*g*(hl.*hl-topol.*topol) - 0.5*(axx.*hl.*Gxul + 2*Gxaxx.*hl.*ul + axx.*ul.*Gxhl) + ul.*(hl.*sigmadBtx)/dt;
FL3=vl.*uNl.*hl;% - 0.5*(axx.*hl.*Gxvl + 2*Gxaxx.*hl.*vl + axx.*vl.*Gxhl) + vl.*(hl.*sigmadBtx)/dt;

% Right flux
FR1=uNr.*hr - 0.5*(axx.*Gxhr + 2*Gxaxx.*hr) + (hr.*sigmadBtx)/dt;
FR2=ur.*uNr.*hr + 0.5*g*(hr.*hr-topor.*topor) - 0.5*(axx.*hr.*Gxur + 2*Gxaxx.*hr.*ur + axx.*ur.*Gxhr) + ur.*(hr.*sigmadBtx)/dt;
FR3=vr.*uNr.*hr;% - 0.5*(axx.*hr.*Gxvr + 2*Gxaxx.*hr.*vr + axx.*vr.*Gxhr) + vr.*(hr.*sigmadBtx)/dt;

else
% Left flux
FL1=uNl.*hl;% - 0.5*axx.*Gxhl - Gxaxx.*hl;
FL2=ul.*uNl.*hl + 0.5*g*(hl.*hl-topol.*topol) - 0.5*axx.*hl.*Gxul - 0.5*axx.*ul.*Gxhl;% - Gxaxx.*hl.*ul;
FL3=vl.*uNl.*hl;% - 0.5*(axx.*hl.*Gxvl);% + axx.*vl.*Gxhl);% + 2*Gxaxx.*hl.*vl);

% Right flux
FR1=uNr.*hr;% - 0.5*axx.*Gxhr - Gxaxx.*hr;
FR2=ur.*uNr.*hr + 0.5*g*(hr.*hr-topor.*topor) - 0.5*axx.*hr.*Gxur - 0.5*axx.*ur.*Gxhr;% - Gxaxx.*hr.*ur;
FR3=vr.*uNr.*hr;% - 0.5*(axx.*hr.*Gxvr);% + axx.*vr.*Gxhr);% + 2*Gxaxx.*hr.*vr); 
% 
% a=uNl.*hl;
% b=-0.5*axx.*Gxhl;
% c=-Gxaxx.*hl;
% d=a+b+c;
% plot(a(1,:));
% hold on
% plot(b(1,:),'k');
% plot(c(1,:),'--k');
% plot(d(1,:),'g');
% hold off
% % 
% figure;
% a=ul.*uNl.*hl;
% b=0.5*g*(hl.*hl-topol.*topol);
% c=- 0.5*axx.*hl.*Gxul;
% d=- 0.5*axx.*ur.*Gxhr;
% e=- Gxaxx.*hr.*ur;
% f=a+b+c+d;
% plot(a(1,:))
% hold on
% plot(b(1,:),'r');
% plot(c(1,:),'k');
% plot(d(1,:),':k');
% plot(e(1,:),'--k');
% plot(f(1,:),'g');
% hold off

end
end

% Roe flux
F(:,:,1)=0.5*(FL1+FR1-alpha2-alpha3);
F(:,:,2)=0.5*(FL2+FR2-(R21.*alpha1)-(R22.*alpha2)-(R23.*alpha3));
F(:,:,3)=0.5*(FL3+FR3-(R31.*alpha1)-(R32.*alpha2)-(R33.*alpha3));
end