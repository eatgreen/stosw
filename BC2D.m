% Boundary conditions test
% Periodic boundary conditions

function [XBC,X0BC] = BC2D(X,option)

dynstep=option.dynstep;
% X0=option.X0;
switch option.BC
    case 'Dirichlet'
                h=X(:,:,1);u=X(:,:,2);v=X(:,:,3);
                nx=size(h,1); ny=size(h,2);
                X0BC.XE=[];X0BC.XW=[];X0BC.Xc=[];X0BC.XN=[];X0BC.XS=[];
                
                % West values ------------------------------
                XBC.XW(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
                XBC.XW(:,:,2)=[zeros(nx,1) u ; 0 u(end,:)];
                XBC.XW(:,:,3)=[zeros(nx,1) v ; 0 v(end,:)];
                 % East values ------------------------------
                XBC.XE(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XE(:,:,2)=[u zeros(nx,1) ; u(end,:) 0];
                XBC.XE(:,:,3)=[v zeros(nx,1) ; v(end,:) 0];
                % North values ------------------------------
                XBC.XN(:,:,1)=[h(1,:) h(1,end) ;h h(:,end)] ;
                XBC.XN(:,:,2)=[zeros(1,ny) 0 ;u u(:,end)];
                XBC.XN(:,:,3)=[zeros(1,ny) 0 ;v v(:,end)];
                % South values ------------------------------
                XBC.XS(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XS(:,:,2)=[u u(:,end) ; zeros(1,ny) 0];
                XBC.XS(:,:,3)=[v v(:,end) ; zeros(1,ny) 0];
       case 'Newmon'
                h=X(:,:,1);u=X(:,:,2);v=X(:,:,3);
                nx=size(h,1); ny=size(h,2);
                X0BC.XE=[];X0BC.XW=[];X0BC.Xc=[];X0BC.XN=[];X0BC.XS=[];
                
                % West values ------------------------------
                XBC.XW(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
                XBC.XW(:,:,2)=[u(:,1)  u ; u(end,1) u(end,:)];
                XBC.XW(:,:,3)=[v(:,1)  v ; v(end,1) v(end,:)];
                 % East values ------------------------------
                XBC.XE(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XE(:,:,2)=[u u(:,end) ; u(end,:) u(end,end)];
                XBC.XE(:,:,3)=[v v(:,end) ; v(end,:) v(end,end)];
                % North values ------------------------------
                XBC.XN(:,:,1)=[h(1,:) h(1,end) ;h h(:,end)] ;
                XBC.XN(:,:,2)=[u(1,:) u(1,end) ;u u(:,end)];
                XBC.XN(:,:,3)=[v(1,:) v(1,end) ;v v(:,end)];
                % South values ------------------------------
                XBC.XS(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XS(:,:,2)=[u u(:,end) ; u(end,:) u(end,end)];
                XBC.XS(:,:,3)=[v v(:,end) ; v(end,:) v(end,end)];
                
    case 'ChannelFlow'
                h=X(:,:,1);u=X(:,:,2);v=X(:,:,3);
                nx=size(h,1); ny=size(h,2);
                X0BC.XE=[];X0BC.XW=[];X0BC.Xc=[];X0BC.XN=[];X0BC.XS=[];
                
                % West values ------------------------------
                XBC.XW(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
                XBC.XW(:,:,2)=[zeros(nx,1)  u ; 0 u(end,:)];
                XBC.XW(:,:,3)=[v(:,1)  v ; v(end,1) v(end,:)];
         
                 % East values ------------------------------
                XBC.XE(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XE(:,:,2)=[u zeros(nx,1) ; u(end,:) 0];
                XBC.XE(:,:,3)=[v v(:,end) ; v(end,:) v(end,end)];
                
                % North values ------------------------------
                XBC.XN(:,:,1)=[h(1,:) h(1,end) ;h h(:,end)] ;
                XBC.XN(:,:,2)=[u(1,:) u(1,end) ;u u(:,end)];
                XBC.XN(:,:,3)=[v(1,:) v(1,end) ;v v(:,end)];
                
                % South values ------------------------------
                XBC.XS(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XS(:,:,2)=[u u(:,end) ; u(end,:) u(end,end)];
                XBC.XS(:,:,3)=[v v(:,end) ; u(end,:) u(end,end)];
 
                
    % Reflective boundary conditions ===============================
    case 'Reflective'
        switch dynstep
            % Dynamic boundary conditions ========================
            case 'dyn'
                h=X(:,:,1);u=X(:,:,2);v=X(:,:,3);
                X0BC.XE=[];X0BC.XW=[];X0BC.Xc=[];X0BC.XN=[];X0BC.XS=[];
                
                % East values ------------------------------
                XBC.XE(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XE(:,:,2)=[u -u(:,end) ; -u(end,:) -u(end,end)];
                XBC.XE(:,:,3)=[v -v(:,end) ; -v(end,:) -v(end,end)];
                % West values ------------------------------
                XBC.XW(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
                XBC.XW(:,:,2)=[-u(:,1) u ; -u(end,1) -u(end,:)];
                XBC.XW(:,:,3)=[-v(:,1)  v; -v(end,1) -v(end,:)];
                % North values ------------------------------
                XBC.XN(:,:,1)=[h(1,:) h(1,end) ;h h(:,end)] ;
                XBC.XN(:,:,2)=[u(1,:) u(1,end) ;u u(:,end)];
                XBC.XN(:,:,3)=[v(1,:) v(1,end) ;v v(:,end)];
                
                % South values ------------------------------
                XBC.XS(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XS(:,:,2)=[u u(:,end) ; u(end,:) u(end,end)];
                XBC.XS(:,:,3)=[v v(:,end) ; u(end,:) u(end,end)];
                
            case 'tan'
                h=X(:,:,1);u=X(:,:,2);v=X(:,:,3);
                X0=foo1(option.X0);
                h0=X0(:,:,1);u0=X0(:,:,2);v0=X0(:,:,3);
                
                % Linearized values =======================================
                % East values ------------------------------
                X0BC.XE(:,:,1)=[h0 h0(:,end) ; h0(end,:) h0(end,end)];
                X0BC.XE(:,:,2)=[u0 -u0(:,end) ; -u0(end,:) -u0(end,end)];
                X0BC.XE(:,:,3)=[v0 -v0(:,end) ; -v0(end,:) -v0(end,end)];
                % West values ------------------------------
                X0BC.XW(:,:,1)=[h0(:,1)  h0 ; h0(end,1) h0(end,:)];
                X0BC.XW(:,:,2)=[-u0(:,1) u0 ; -u0(end,1) -u0(end,:)];
                X0BC.XW(:,:,3)=[-v0(:,1)  v0; -v0(end,1) -v0(end,:)];
                % Northpvalues ------------------------------
                X0BC.XN(:,:,1)=[h0(1,:) h0(1,end) ;h0 h0(:,end)] ;
                X0BC.XN(:,:,2)=[-u0(1,:) -u0(1,end) ;u0 -u0(:,end)];
                X0BC.XN(:,:,3)=[-v0(1,:) -v0(1,end) ;v0 -v0(:,end)];
                % South values ------------------------------
                X0BC.XS(:,:,1)=[h0 h0(:,end) ; h0(end,:) h0(end,end)];
                X0BC.XS(:,:,2)=[u0 -u0(:,end) ; -u0(end,:) -u0(end,end)];
                X0BC.XS(:,:,3)=[v0 -v0(:,end) ; -v0(end,:) -v0(end,end)];
                
                % East values ------------------------------
                XBC.XE(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XE(:,:,2)=[u -u(:,end) ; -u(end,:) -u(end,end)];
                XBC.XE(:,:,3)=[v -v(:,end) ; -v(end,:) -v(end,end)];
                % West values ------------------------------
                XBC.XW(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
                XBC.XW(:,:,2)=[-u(:,1) u ; -u(end,1) -u(end,:)];
                XBC.XW(:,:,3)=[-v(:,1)  v; -v(end,1) -v(end,:)];
                % North values ------------------------------
                XBC.XN(:,:,1)=[h(1,:) h(1,end) ;h h(:,end)] ;
                XBC.XN(:,:,2)=[-u(1,:) -u(1,end) ;u -u(:,end)];
                XBC.XN(:,:,3)=[-v(1,:) -v(1,end) ;v -v(:,end)];
                % South values ------------------------------
                XBC.XS(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XS(:,:,2)=[u -u(:,end) ; -u(end,:) -u(end,end)];
                XBC.XS(:,:,3)=[v -v(:,end) ; -v(end,:) -v(end,end)];
            case 'adj'
                %                 X0BC.XE=[];X0BC.XW=[];X0BC.Xc=[];X0BC.XN=[];X0BC.XS=[];
                
                % East values ------------------------------
                XE=X.XE;
                he=XE(:,:,1); ue=XE(:,:,2); ve=XE(:,:,3);
                hb=he(1:end-1,1:end-1);
                hb(:,end)=hb(:,end)+he(1:end-1,end);
                hb(end,:)=hb(end,:)+he(end,1:end-1);
                hb(end,end)=hb(end,end)+he(end,end);
                
                ub=ue(1:end-1,1:end-1);
                ub(:,end)=ub(:,end)-ue(1:end-1,end);
                ub(end,:)=ub(end,:)-ue(end,1:end-1);
                ub(end,end)=ub(end,end)-ue(end,end);
                
                vb=ve(1:end-1,1:end-1);
                vb(:,end)=vb(:,end)-ve(1:end-1,end);
                vb(end,:)=vb(end,:)-ve(end,1:end-1);
                vb(end,end)=vb(end,end)-ve(end,end);
                
                clear he ue ve
                
                % West values ------------------------------
                XW=X.XW;
                hw=XW(:,:,1); uw=XW(:,:,2); vw=XW(:,:,3);
                hb=hb+hw(1:end-1,2:end);
                hb(:,1)=hb(:,1)+hw(1:end-1,1);
                hb(end,:)=hb(end,:)+hw(end,2:end);
                hb(end,1)=hb(end,1)+hw(end,1);
                
                ub=ub+uw(1:end-1,2:end);
                ub(:,1)=ub(:,1)-uw(1:end-1,1);
                ub(end,:)=ub(end,:)-uw(end,2:end);
                ub(end,1)=ub(end,1)-uw(end,1);
                
                vb=vb+vw(1:end-1,2:end);
                vb(:,1)=vb(:,1)-vw(1:end-1,1);
                vb(end,:)=vb(end,:)-vw(end,2:end);
                vb(end,1)=vb(end,1)-vw(end,1);
                
                clear hw uw vw
                
                % North values ------------------------------
                XN=X.XN;
                hn=XN(:,:,1); un=XN(:,:,2); vn=XN(:,:,3);
                hb=hb+hn(2:end,1:end-1);
                hb(:,end)=hb(:,end)+hn(2:end,end);
                hb(1,:)=hb(1,:)+hn(1,1:end-1);
                hb(1,end)=hb(1,end)+hn(1,end);
                
                ub=ub+un(2:end,1:end-1);
                ub(:,end)=ub(:,end)-un(2:end,end);
                ub(1,:)=ub(1,:)-un(1,1:end-1);
                ub(1,end)=ub(1,end)-un(1,end);
                
                vb=vb+vn(2:end,1:end-1);
                vb(:,end)=vb(:,end)-vn(2:end,end);
                vb(1,:)=vb(1,:)-vn(1,1:end-1);
                vb(1,end)=vb(1,end)-vn(1,end);
                
                clear hn un vn
                
                % South values ------------------------------
                XS=X.XS;
                hs=XS(:,:,1); us=XS(:,:,2); vs=XS(:,:,3);
                hb=hb+hs(1:end-1,1:end-1);
                hb(:,end)=hb(:,end)+hs(1:end-1,end);
                hb(end,:)=hb(end,:)+hs(end,1:end-1);
                hb(end,end)=hb(end,end)+hs(end,end);
                
                ub=ub+us(1:end-1,1:end-1);
                ub(:,end)=ub(:,end)-us(1:end-1,end);
                ub(end,:)=ub(end,:)-us(end,1:end-1);
                ub(end,end)=ub(end,end)-us(end,end);
                
                vb=vb+vs(1:end-1,1:end-1);
                vb(:,end)=vb(:,end)-vs(1:end-1,end);
                vb(end,:)=vb(end,:)-vs(end,1:end-1);
                vb(end,end)=vb(end,end)-vs(end,end);
                
                clear hs us vs
                
                XBC.Xb(:,:,1)=hb;
                XBC.Xb(:,:,2)=ub;
                XBC.Xb(:,:,3)=vb;
                
        end
        %% Channel
    case 'channel'
        switch step
            case 'dyn'
                h=X(:,:,1);u=X(:,:,2);v=X(:,:,3);
                [n,m]=size(h);
                % East values ------------------------------
                XBC.XE(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XE(:,:,2)=[u -u(:,end) ; u(end,:) 0];
                XBC.XE(:,:,3)=[v v(:,end) ; zeros(1,m+1)];
                % West values ------------------------------
                XBC.XW(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
                XBC.XW(:,:,2)=[option.inflow(:,1,2)*option.vanne u ; 0 u(end,:)];
                XBC.XW(:,:,3)=[zeros(n,1)  v; zeros(1,m+1)];
                % North values ------------------------------
                XBC.XN(:,:,1)=[h(1,:) h(1,end) ;h h(:,end)] ;
                XBC.XN(:,:,2)=[u(1,:) 0 ;u -u(:,end)];
                XBC.XN(:,:,3)=[zeros(1,m+1) ;v v(:,end)];
                % South values ------------------------------
                XBC.XS(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XS(:,:,2)=[u -u(:,end) ; u(end,:) 0];
                XBC.XS(:,:,3)=[v v(:,end) ; zeros(1,m+1)];
                
            case 'tan'
                h=X(:,:,1);u=X(:,:,2);v=X(:,:,3);
                [n,m]=size(h);
                X0=foo1(option.X0);
                h0=X0(:,:,1);u0=X0(:,:,2);v0=X0(:,:,3);
                % East values ------------------------------
                X0BC.XE(:,:,1)=[h0 h0(:,end) ; h0(end,:) h0(end,end)];
                X0BC.XE(:,:,2)=[u0 -u0(:,end) ; u0(end,:) 0];
                X0BC.XE(:,:,3)=[v0 v0(:,end) ; zeros(1,m+1)];
                % West values ------------------------------
                X0BC.XW(:,:,1)=[h0(:,1)  h0 ; h0(end,1) h0(end,:)];
                X0BC.XW(:,:,2)=[option.inflow(:,1,2)*option.vanne u0 ; 0 u0(end,:)];
                X0BC.XW(:,:,3)=[zeros(n,1)  v0; zeros(1,m+1)];
                % North values ------------------------------
                X0BC.XN(:,:,1)=[h0(1,:) h0(1,end) ;h0 h0(:,end)] ;
                X0BC.XN(:,:,2)=[u0(1,:) 0 ;u0 -u0(:,end)];
                X0BC.XN(:,:,3)=[zeros(1,m+1) ;v0 v0(:,end)];
                % Sou0th values ------------------------------
                X0BC.XS(:,:,1)=[h0 h0(:,end) ; h0(end,:) h0(end,end)];
                X0BC.XS(:,:,2)=[u0 -u0(:,end) ; u0(end,:) 0];
                X0BC.XS(:,:,3)=[v0 v0(:,end) ; zeros(1,m+1)];
                
                % East values ------------------------------
                XBC.XE(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XE(:,:,2)=[u -u(:,end) ; u(end,:) 0];
                XBC.XE(:,:,3)=[v v(:,end) ; zeros(1,m+1)];
                % West values ------------------------------
                XBC.XW(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
                XBC.XW(:,:,2)=[option.inflow(:,1,2)*option.vanne u ; 0 u(end,:)];
                XBC.XW(:,:,3)=[zeros(n,1)  v; zeros(1,m+1)];
                % North values ------------------------------
                XBC.XN(:,:,1)=[h(1,:) h(1,end) ;h h(:,end)] ;
                XBC.XN(:,:,2)=[u(1,:) 0 ;u -u(:,end)];
                XBC.XN(:,:,3)=[zeros(1,m+1) ;v v(:,end)];
                % South values ------------------------------
                XBC.XS(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                XBC.XS(:,:,2)=[u -u(:,end) ; u(end,:) 0];
                XBC.XS(:,:,3)=[v v(:,end) ; zeros(1,m+1)];
                
            case 'adj'
                
                % East values ------------------------------
                XE=X.XE;
                he=XE(:,:,1); ue=XE(:,:,2); ve=XE(:,:,3);
                hb=he(1:end-1,1:end-1);
                hb(:,end)=hb(:,end)+he(1:end-1,end);
                hb(end,:)=hb(end,:)+he(end,1:end-1);
                hb(end,end)=hb(end,end)+he(end,end);
                
                ub=ue(1:end-1,1:end-1);
                ub(:,end)=ub(:,end)-ue(1:end-1,end);
                ub(end,:)=ub(end,:)+ue(end,1:end-1);
                %                 ub(end,end)=ub(end,end)+ue(end,end);
                
                vb=ve(1:end-1,1:end-1);
                vb(:,end)=vb(:,end)+ve(1:end-1,end);
                %                 vb(end,:)=vb(end,:)+ve(end,1:end-1);
                %                 vb(end,end)=vb(end,end)+ve(end,end);
                
                clear he ue ve
                
                % West values ------------------------------
                XW=X.XW;
                hw=XW(:,:,1); uw=XW(:,:,2); vw=XW(:,:,3);
                hb=hb+hw(1:end-1,2:end);
                hb(:,1)=hb(:,1)+hw(1:end-1,1);
                hb(end,:)=hb(end,:)+hw(end,2:end);
                hb(end,1)=hb(end,1)+hw(end,1);
                
                ub=ub+uw(1:end-1,2:end);
                ub(:,1)=ub(:,1)+option.inflow(:,1,2)*option.vanne;
                ub(end,:)=ub(end,:)+uw(end,2:end);
                % ub(end,1)=ub(end,1)-uw(end,1);
                
                vb=vb+vw(1:end-1,2:end);
                % vb(:,1)=vb(:,1)+vw(1:end-1,1);
                % vb(end,:)=vb(end,:)+vw(end,2:end);
                % vb(end,1)=vb(end,1)+vw(end,1);
                
                clear hw uw vw
                
                % North values ------------------------------
                XN=X.XN;
                hn=XN(:,:,1); un=XN(:,:,2); vn=XN(:,:,3);
                hb=hb+hn(2:end,1:end-1);
                hb(:,end)=hb(:,end)+hn(2:end,end);
                hb(1,:)=hb(1,:)+hn(1,1:end-1);
                hb(1,end)=hb(1,end)+hn(1,end);
                
                ub=ub+un(2:end,1:end-1);
                ub(:,end)=ub(:,end)-un(2:end,end);
                ub(1,:)=ub(1,:)+un(1,1:end-1);
                %                 ub(1,end)=ub(1,end)+un(1,end);
                
                vb=vb+vn(2:end,1:end-1);
                vb(:,end)=vb(:,end)+vn(2:end,end);
                % vb(1,:)=vb(1,:)-vn(1,1:end-1);
                % vb(1,end)=vb(1,end)-vn(1,end);
                
                clear hn un vn
                
                % South values ------------------------------
                XS=X.XS;
                hs=XS(:,:,1); us=XS(:,:,2); vs=XS(:,:,3);
                hb=hb+hs(1:end-1,1:end-1);
                hb(:,end)=hb(:,end)+hs(1:end-1,end);
                hb(end,:)=hb(end,:)+hs(end,1:end-1);
                hb(end,end)=hb(end,end)+hs(end,end);
                
                ub=ub+us(1:end-1,1:end-1);
                ub(:,end)=ub(:,end)-us(1:end-1,end);
                ub(end,:)=ub(end,:)+us(end,1:end-1);
                %                 ub(end,end)=ub(end,end)+us(end,end);
                
                vb=vb+vs(1:end-1,1:end-1);
                vb(:,end)=vb(:,end)+vs(1:end-1,end);
                % vb(end,:)=vb(end,:)-vs(end,1:end-1);
                % vb(end,end)=vb(end,end)-vs(end,end);
                
                clear hs us vs
                
                XBC.Xb(:,:,1)=hb;
                XBC.Xb(:,:,2)=ub;
                XBC.Xb(:,:,3)=vb;
                
                %                 h=X(:,:,1);u=X(:,:,2);v=X(:,:,3);
                %                 [n,m]=size(h);
                %                 % East values ------------------------------
                %                 X0BC.XE(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                %                 X0BC.XE(:,:,2)=[u -u(:,end) ; -u(end,:) -u(end,end)];
                %                 X0BC.XE(:,:,3)=[v -v(:,end) ; -v(end,:) -v(end,end)];
                %                 % West values ------------------------------
                %                 X0BC.XW(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
                %                 X0BC.XW(:,:,2)=[option.inflow(:,1,2)*option.vanne u ; zeros(1,m+1)];
                %                 X0BC.XW(:,:,3)=[zeros(n,1)  v; zeros(1,m+1)];
                %                 % North values ------------------------------
                %                 X0BC.XN(:,:,1)=[h(1,:) h(1,end) ;h h(:,end)] ;
                %                 X0BC.XN(:,:,2)=[u(1,:) u(1,end) ;u zeros(n,1)];
                %                 X0BC.XN(:,:,3)=[zeros(1,m+1) ;v v(:,end)];
                %                 % South values ------------------------------
                %                 X0BC.XS(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                %                 X0BC.XS(:,:,2)=[u zeros(n,1) ; u(end,:) u(end,end)];
                %                 X0BC.XS(:,:,3)=[v v(:,end) ; zeros(1,m+1)];
                %
                %
                %                 % East values ------------------------------
                %                 XBC.XW(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                %                 XBC.XW(:,:,2)=[u -u(:,end) ; -u(end,:) -u(end,end)];
                %                 XBC.XW(:,:,3)=[v -v(:,end) ; -v(end,:) -v(end,end)];
                %                 % West values ------------------------------
                %                 XBC.XE(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
                %                 XBC.XE(:,:,2)=[option.inflow(:,1,2)*option.vanne u ; zeros(1,m+1)];
                %                 XBC.XE(:,:,3)=[zeros(n,1)  v; zeros(1,m+1)];
                %                 % North values ------------------------------
                %                 XBC.XS(:,:,1)=[h(1,:) h(1,end) ;h h(:,end)] ;
                %                 XBC.XS(:,:,2)=[u(1,:) u(1,end) ;u zeros(n,1)];
                %                 XBC.XS(:,:,3)=[zeros(1,m+1) ;v v(:,end)];
                %                 % South values ------------------------------
                %                 XBC.XN(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
                %                 XBC.XN(:,:,2)=[u zeros(n,1) ; u(end,:) u(end,end)];
                %                 XBC.XN(:,:,3)=[v v(:,end) ; zeros(1,m+1)];
        end
        
    case 'benoit'
        
        % Dynamic boundary conditions ========================
        h=X(:,:,1);u=X(:,:,2);v=X(:,:,3);
        X0BC.XE=[];X0BC.XW=[];X0BC.Xc=[];X0BC.XN=[];X0BC.XS=[];
        
        % East values ------------------------------
        XBC.XE(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
        XBC.XE(:,:,2)=[u zeros(nx,1) ; u(end,:) 0];
        XBC.XE(:,:,3)=[v v(:,end) ; v(end,:) v(end,end)];
        % West values ------------------------------
        XBC.XW(:,:,1)=[h(:,1)  h ; h(end,1) h(end,:)];
        XBC.XW(:,:,2)=[zeros(nx,1) u ; 0 u(end,:)];
        XBC.XW(:,:,3)=[v(:,1)  v; v(end,1) v(end,:)];
        % North values -----------------------------
        XBC.XN(:,:,1)=[option.inflow(1)*ones(1,ny) h(1,end) ;h h(:,end)] ;
        XBC.XN(:,:,2)=[option.inflow(2)*ones(1,ny) u(1,end) ;u u(:,end)];
        XBC.XN(:,:,3)=[option.inflow(3)*ones(1,ny) v(1,end) ;v v(:,end)];
        % South values ------------------------------
        XBC.XS(:,:,1)=[h h(:,end) ; h(end,:) h(end,end)];
        XBC.XS(:,:,2)=[u u(:,end) ; u(end,:) u(end,end)];
        XBC.XS(:,:,3)=[v v(:,end) ; v(end,:) v(end,end)];
        
    otherwise
        disp('Error: Unknown boundary condition name');
        return
end