function S=SourceTerm(hw,he,hn,hs,uw,ue,un,us,vw,ve,vn,vs,topo,cori,bofr)

g=9.806;

[nx,ny]=size(topo.topo);
S=zeros(nx,ny,2);
h=he(1:(end-1),1:(end-1));
u=ue(1:(end-1),1:(end-1));
v=ve(1:(end-1),1:(end-1));

if strcmp(topo.flag,'On')
% S(:,:,1)=-0.5*g.*(hw(1:(end-1),2:end)+he(1:(end-1),2:end)).*topo.Hx;
% S(:,:,2)=-0.5*g.*(hn(2:end,1:(end-1))+hs(2:end,1:(end-1))).*topo.Hy;

S(:,:,1)=-g.*h.*topo.Hx;
% S(:,:,2)=-g.*h.*topo.Hy;
end

if strcmp(cori.flag,'On')
% S(:,:,1)=S(:,:,1)+0.5*(hn(1:(end-1),2:end)+hs(1:(end-1),2:end)).*cori.f_cori*0.5.*(vn(2:end,1:(end-1))+vs(2:end,1:(end-1)));
% S(:,:,2)=S(:,:,2)-0.5*(hw(2:end,1:(end-1))+he(2:end,1:(end-1))).*cori.f_cori*0.5.*(uw(1:(end-1),2:end)+ue(1:(end-1),2:end));    
S(:,:,1)=S(:,:,1)+h.*cori.f_cori.*v;
S(:,:,2)=S(:,:,2)-h.*cori.f_cori.*u;    
end

if strcmp(bofr.flag,'On')
end
