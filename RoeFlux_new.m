% Roe flux ============================================================
function F = RoeFlux_new(hl,hr,ul,ur,vl,vr,flag,locunc_term,viscosity_term,topol,topor)

flagX=flag;
flagY=~flag;
locunc_flag=locunc_term.flag;
whitenoise_flag=locunc_term.whitenoise;
viscosity_flag=viscosity_term.flag;

F=zeros(size(hl,1),size(hl,2),3);
%initial parameter
g=9.806;

% Roe averages ------------------
duml=hl.^0.5;
dumr=hr.^0.5;

hhat=duml.*dumr;
uhat=(duml.*ul + dumr.*ur)./(duml+dumr);
vhat=(duml.*vl + dumr.*vr)./(duml+dumr);

chat=((0.5*g*(hl+hr)).^0.5);
chatl=((g*hl).^0.5);
chatr=((g*hr).^0.5);

dh=hr-hl;
du=ur-ul;
dv=vr-vl;

uNl = ul.*flagX + vl.*flagY;
uNr = ur.*flagX + vr.*flagY;

% Eigenvalues --------------------
al2=uNl+chatl;
al3=uNl-chatl;
ar2=uNr+chatr;
ar3=uNr-chatr;

a1 = abs(uhat*flagX + vhat*flagY) ;
a2 = abs(uhat*flagX + vhat*flagY + chat);
a3 = abs(uhat*flagX + vhat*flagY - chat);

% Entropy correction?
da2=max(0,2*(ar2-al2));
da3=max(0,2*(ar3-al3));

tmpIndex=a2<da2;
a2(tmpIndex)=0.5*(a2(tmpIndex).*a2(tmpIndex)./da2(tmpIndex)+da2(tmpIndex));

tmpIndex2=a3<da3;
a3(tmpIndex2)=0.5*(a3(tmpIndex2).*a3(tmpIndex2)./da3(tmpIndex2)+da3(tmpIndex2));

% Right eigenvectors ----------------
R21=-flagY;
R22=uhat+flagX*chat;
R23=uhat-flagX*chat;
R31=flagX;
R32=vhat+flagY*chat;
R33=vhat-flagY*chat;

% Wave coefficients x eigenvalues
% alpha1 = (hhat.*dv*flagX - hhat.*du*flagY).*a1;
% alpha2 = 0.5 * (dh + hhat.*du*flagX + hhat.*dv*flagY)./chat.*a2;
% alpha3 = 0.5 * (dh - hhat.*du*flagX - hhat.*dv*flagY)./chat.*a3;

alpha1 = (hhat.*dv*flagX - hhat.*du*flagY).*a1;
alpha2 = 0.5 * (dh + (hhat.*du*flagX + hhat.*dv*flagY)./chat).*a2;
alpha3 = 0.5 * (dh - (hhat.*du*flagX + hhat.*dv*flagY)./chat).*a3;

%This source term is rather complex
% if strcmp(source_flag,'On')
% Hx=source_term.Hx;
% Hy=source_term.Hy;
%     
% Gx=0.5*g.*(hl+hr).*Hx*flagX;
% Gy=0.5*g.*(hl+hr).*Hy*flagY;
% 
% %Calculate source term in terms of bottom depth variation
% ia1=ones(size(a1))-a1./(uhat*flagX + vhat*flagY);
% ia2=ones(size(a2))-a2./(uhat*flagX + vhat*flagY + chat);
% ia3=ones(size(a3))-a3./(uhat*flagX + vhat*flagY - chat);
% 
% beta1=0.5*(2*Gy*flagX-2*Gx*flagY)./chat.*ia1;
% beta2=0.5*(Gx*flagX+Gy*flagY)./chat.*ia2;
% beta3=0.5*(-Gx*flagX-Gy*flagY)./chat.*ia3;
% 
% S(:,:,1)=beta2+bata3;
% S(:,:,2)=R21.*beta1+R22.*beta2+R23.*beta3;
% S(:,:,3)=R31.*beta1+R32.*beta2+R33.*beta3;
% end

if strcmp(locunc_flag,'Off')
    
if strcmp(viscosity_flag,'Off')
% Left flux
FL1=uNl.*hl;
FL2=ul.*uNl.*hl + 0.5*g.*(hl.*hl-topol.*topol)*flagX;
FL3=vl.*uNl.*hl + 0.5*g.*(hl.*hl-topol.*topol)*flagY;

% Right flux
FR1=uNr.*hr;
FR2=ur.*uNr.*hr + 0.5*g.*(hr.*hr-topor.*topor)*flagX;
FR3=vr.*uNr.*hr + 0.5*g.*(hr.*hr-topor.*topor)*flagY;

else
dx=viscosity_term.dx;
dy=viscosity_term.dy;
% miu=viscosity_term.miu;

[Gxul,Gyul]=gradient(ul,dx,dy);
[Gxvl,Gyvl]=gradient(vl,dx,dy);
[Gxur,Gyur]=gradient(ur,dx,dy);
[Gxvr,Gyvr]=gradient(vr,dx,dy);

miu=dx*dy*sqrt(Gxul.^2+Gyvl.^2+0.5*(Gyul+Gxvl).^2);
% Left flux
FL1=uNl.*hl;
FL2=ul.*uNl.*hl + 0.5*g.*(hl.*hl-topol.*topol)*flagX - (miu.*hl.*Gxul*flagX + miu.*hl.*Gyul*flagY);
FL3=vl.*uNl.*hl + 0.5*g.*(hl.*hl-topol.*topol)*flagY - (miu.*hl.*Gxvl*flagX + miu.*hl.*Gyvl*flagY);

% Right flux
FR1=uNr.*hr;
FR2=ur.*uNr.*hr + 0.5*g.*(hr.*hr-topor.*topor)*flagX - (miu.*hr.*Gxur*flagX + miu.*hr.*Gyur*flagY);
FR3=vr.*uNr.*hr + 0.5*g.*(hr.*hr-topor.*topor)*flagY - (miu.*hr.*Gxvr*flagX + miu.*hr.*Gyvr*flagY);

end

else
dt=locunc_term.dt;
dx=locunc_term.dx;
dy=locunc_term.dy;

sigmaxx=locunc_term.sigmaxx;
sigmaxy=locunc_term.sigmaxy;
sigmayy=locunc_term.sigmayy;

sigmaxx=[sigmaxx sigmaxx(:,end) ;sigmaxx(end,:) sigmaxx(end,end)];
sigmaxy=[sigmaxy sigmaxy(:,end) ;sigmaxy(end,:) sigmaxy(end,end)];
sigmayy=[sigmayy sigmayy(:,end) ;sigmayy(end,:) sigmayy(end,end)];

[Gxhl,Gyhl]=gradient(hl,dx,dy);
[Gxul,Gyul]=gradient(ul,dx,dy);
[Gxvl,Gyvl]=gradient(vl,dx,dy);
[Gxhr,Gyhr]=gradient(hr,dx,dy);
[Gxur,Gyur]=gradient(ur,dx,dy);
[Gxvr,Gyvr]=gradient(vr,dx,dy);

% Construct the diffusion operator
% sigmaxx=1-Ghx.*Ghx/(norm([Ghx,Ghx])^2);
% sigmaxy=sigmaxx-1;
% sigmayy=sigmaxx;
 
axx=sigmaxx.^2+sigmaxy.^2;
% axy=sigmaxx.*sigmaxy+sigmaxy.*sigmayy;
ayy=sigmaxy.^2+sigmayy.^2;
[Gxaxx,~]=gradient(axx,dx,dy);
[~,Gyayy]=gradient(ayy,dx,dy);

% axx=2*dx*dy*sqrt(Gxul.^2+Gyvl.^2+0.5*(Gyul+Gxvl).^2);
% ayy=axx;
% [Gxaxx,~]=gradient(axx,dx,dy);
% [~,Gyayy]=gradient(ayy,dx,dy);

% Standard brownian motion process
if strcmp(whitenoise_flag,'On')
dBtx=sqrt(dt)*randn;
dBty=sqrt(dt)*randn;
sigmadBtx=sigmaxx*dBtx+sigmaxy*dBty;
sigmadBty=sigmaxy*dBtx+sigmayy*dBty;

% Left flux
FL1=uNl.*hl + (hl.*sigmadBtx*flagX+hl.*sigmadBty*flagY)/dt - 0.5*(axx.*Gxhl*flagX + ayy.*Gyhl*flagY + 2*Gxaxx.*hl*flagX + 2*Gyayy.*hl*flagY) ;
FL2=ul.*uNl.*hl + 0.5*g*hl.*hl*flagX + ul.*(hl.*sigmadBtx*flagX+hl.*sigmadBty*flagY)/dt - 0.5*(axx.*hl.*Gxul*flagX + ayy.*hl.*Gyul*flagY + 2*Gxaxx.*hl.*ul*flagX + 2*Gyayy.*hl.*ul*flagY + axx.*ul.*Gxhl*flagX + ayy.*ul.*Gyhl*flagY);
FL3=vl.*uNl.*hl + 0.5*g*hl.*hl*flagY + vl.*(hl.*sigmadBtx*flagX+hl.*sigmadBty*flagY)/dt - 0.5*(axx.*hl.*Gxvl*flagX + ayy.*hl.*Gyvl*flagY + 2*Gxaxx.*hl.*vl*flagX + 2*Gyayy.*hl.*vl*flagY + axx.*vl.*Gxhl*flagX + ayy.*vl.*Gyhl*flagY);

% Right flux
FR1=uNr.*hr + (hr.*sigmadBtx*flagX+hr.*sigmadBty*flagY)/dt - (axx/2.*Gxhr*flagX + ayy/2.*Gyhr*flagY + 2*Gxaxx.*hr*flagX + 2*Gyayy.*hr*flagY);
FR2=ur.*uNr.*hr + 0.5*g*hr.*hr*flagX + ur.*(hr.*sigmadBtx*flagX+hr.*sigmadBty*flagY)/dt - 0.5*(axx.*hr.*Gxur*flagX + ayy.*hr.*Gyur*flagY + 2*Gxaxx.*hr.*ur*flagX + 2*Gyayy.*hr.*ur*flagY + axx.*ur.*Gxhr*flagX + ayy.*ur.*Gyhr*flagY);
FR3=vr.*uNr.*hr + 0.5*g*hr.*hr*flagY + vr.*(hr.*sigmadBtx*flagX+hr.*sigmadBty*flagY)/dt - 0.5*(axx.*hr.*Gxvr*flagX + ayy.*hr.*Gyvr*flagY + 2*Gxaxx.*hr.*vr*flagX + 2*Gyayy.*hr.*vr*flagY + axx.*vr.*Gxhr*flagX + ayy.*vr.*Gyhr*flagY);

else
% Left flux
FL1=uNl.*hl - 0.5*(axx.*Gxhl*flagX + ayy.*Gyhl*flagY + 2*Gxaxx.*hl*flagX + 2*Gyayy.*hl*flagY) ;
FL2=ul.*uNl.*hl + 0.5*g*hl.*hl*flagX - 0.5*(axx.*hl.*Gxul*flagX + ayy.*hl.*Gyul*flagY + 2*Gxaxx.*hl.*ul*flagX + 2*Gyayy.*hl.*ul*flagY + axx.*ul.*Gxhl*flagX + ayy.*ul.*Gyhl*flagY);
FL3=vl.*uNl.*hl + 0.5*g*hl.*hl*flagY - 0.5*(axx.*hl.*Gxvl*flagX + ayy.*hl.*Gyvl*flagY + 2*Gxaxx.*hl.*vl*flagX + 2*Gyayy.*hl.*vl*flagY + axx.*vl.*Gxhl*flagX + ayy.*vl.*Gyhl*flagY);

% Right flux
FR1=uNr.*hr - (axx/2.*Gxhr*flagX + ayy/2.*Gyhr*flagY + 2*Gxaxx.*hr*flagX + 2*Gyayy.*hr*flagY);
FR2=ur.*uNr.*hr + 0.5*g*hr.*hr*flagX - 0.5*(axx.*hr.*Gxur*flagX + ayy.*hr.*Gyur*flagY + 2*Gxaxx.*hr.*ur*flagX + 2*Gyayy.*hr.*ur*flagY + axx.*ur.*Gxhr*flagX + ayy.*ur.*Gyhr*flagY);
FR3=vr.*uNr.*hr + 0.5*g*hr.*hr*flagY - 0.5*(axx.*hr.*Gxvr*flagX + ayy.*hr.*Gyvr*flagY + 2*Gxaxx.*hr.*vr*flagX + 2*Gyayy.*hr.*vr*flagY + axx.*vr.*Gxhr*flagX + ayy.*vr.*Gyhr*flagY); 
end
end

% Roe flux
F(:,:,1)=0.5*(FL1+FR1-alpha2-alpha3);
F(:,:,2)=0.5*(FL2+FR2-(R21.*alpha1)-(R22.*alpha2)-(R23.*alpha3));
F(:,:,3)=0.5*(FL3+FR3-(R31.*alpha1)-(R32.*alpha2)-(R33.*alpha3));
end