function F = Geostrophic(hl,hr,ul,ur,vl,vr,flag)

flagX=flag;
flagY=~flag;

g=9.806;

h=hr(1:(end-1),1:(end-1));

F=zeros(size(h,1),size(h,2),3);

FL1=0;
FL2=0.5*g.*h.*h*flagX;
FL3=0.5*g.*h.*h*flagY;

% Right flux
FR1=0;
FR2=0.5*g.*h.*h*flagX;
FR3=0.5*g.*h.*h*flagY;

% Roe flux
F(:,:,1)=0.5*(FL1+FR1);%-alpha2-alpha3);
F(:,:,2)=0.5*(FL2+FR2);%-(R21.*alpha1)-(R22.*alpha2)-(R23.*alpha3));
F(:,:,3)=0.5*(FL3+FR3);%-(R31.*alpha1)-(R32.*alpha2)-(R33.*alpha3));