% Roe flux ============================================================
function G = RoeFlux_newY(hl,hr,ul,ur,vl,vr,locunc_term,viscosity_term,topol,topor)

locunc_flag=locunc_term.flag;
whitenoise_flag=locunc_term.whitenoise;
viscosity_flag=viscosity_term.flag;

G=zeros(size(hl,1),size(hl,2),3);
%initial parameter
g=9.806;

% Roe averages ------------------
duml=hl.^0.5;
dumr=hr.^0.5;

hhat=duml.*dumr;
uhat=(duml.*ul + dumr.*ur)./(duml+dumr);
vhat=(duml.*vl + dumr.*vr)./(duml+dumr);

chat=((0.5*g*(hl+hr)).^0.5);
chatl=((g*hl).^0.5);
chatr=((g*hr).^0.5);

dh=hr-hl;
du=ur-ul;
dv=vr-vl;

uNl = vl;
uNr = vr;

% Eigenvalues --------------------
al2=uNl+chatl;
al3=uNl-chatl;
ar2=uNr+chatr;
ar3=uNr-chatr;

a1 = abs(vhat) ;
a2 = abs(vhat + chat);
a3 = abs(vhat - chat);

% Entropy correction?
da2=max(0,2*(ar2-al2));
da3=max(0,2*(ar3-al3));

tmpIndex=a2<da2;
a2(tmpIndex)=0.5*(a2(tmpIndex).*a2(tmpIndex)./da2(tmpIndex)+da2(tmpIndex));

tmpIndex2=a3<da3;
a3(tmpIndex2)=0.5*(a3(tmpIndex2).*a3(tmpIndex2)./da3(tmpIndex2)+da3(tmpIndex2));

% Right eigenvectors ----------------
R21=-1;
R22=uhat;
R23=uhat;
R31=0;
R32=vhat+chat;
R33=vhat-chat;

% Wave coefficients x eigenvalues
% alpha1 = (hhat.*dv*flagX - hhat.*du*flagY).*a1;
% alpha2 = 0.5 * (dh + hhat.*du*flagX + hhat.*dv*flagY)./chat.*a2;
% alpha3 = 0.5 * (dh - hhat.*du*flagX - hhat.*dv*flagY)./chat.*a3;

alpha1 = (- hhat.*du).*a1;
alpha2 = 0.5 * (dh + (hhat.*dv)./chat).*a2;
alpha3 = 0.5 * (dh - (hhat.*dv)./chat).*a3;

if strcmp(locunc_flag,'Off')
    
if strcmp(viscosity_flag,'Off')
% Left flux
FL1=0;
FL2=0;
FL3=0;

% Right flux
FR1=0;
FR2=0;
FR3=0;

else
dx=viscosity_term.dx;
dy=viscosity_term.dy;
% miu=viscosity_term.miu;

[Gxul,Gyul]=gradient(ul,dx,dy);
[Gxvl,Gyvl]=gradient(vl,dx,dy);
[Gxur,Gyur]=gradient(ur,dx,dy);
[Gxvr,Gyvr]=gradient(vr,dx,dy);

miu=dx*dy*sqrt(Gxul.^2+Gyvl.^2+0.5*(Gyul+Gxvl).^2);
% Left flux
FL1=uNl.*hl;
FL2=ul.*uNl.*hl + 0.5*g.*(hl.*hl-topol.*topol)*flagX - (miu.*hl.*Gxul*flagX + miu.*hl.*Gyul*flagY);
FL3=vl.*uNl.*hl + 0.5*g.*(hl.*hl-topol.*topol)*flagY - (miu.*hl.*Gxvl*flagX + miu.*hl.*Gyvl*flagY);

% Right flux
FR1=uNr.*hr;
FR2=ur.*uNr.*hr + 0.5*g.*(hr.*hr-topor.*topor)*flagX - (miu.*hr.*Gxur*flagX + miu.*hr.*Gyur*flagY);
FR3=vr.*uNr.*hr + 0.5*g.*(hr.*hr-topor.*topor)*flagY - (miu.*hr.*Gxvr*flagX + miu.*hr.*Gyvr*flagY);

end

else
dt=locunc_term.dt;
dx=locunc_term.dx;
dy=locunc_term.dy;

sigmaxx=locunc_term.sigmaxx;
sigmaxy=locunc_term.sigmaxy;
sigmayy=locunc_term.sigmayy;

sigmaxx=[sigmaxx sigmaxx(:,end) ;sigmaxx(end,:) sigmaxx(end,end)];
sigmaxy=[sigmaxy sigmaxy(:,end) ;sigmaxy(end,:) sigmaxy(end,end)];
sigmayy=[sigmayy sigmayy(:,end) ;sigmayy(end,:) sigmayy(end,end)];

[Gxhl,Gyhl]=gradient(hl,dx,dy);
[Gxul,Gyul]=gradient(ul,dx,dy);
[Gxvl,Gyvl]=gradient(vl,dx,dy);
[Gxhr,Gyhr]=gradient(hr,dx,dy);
[Gxur,Gyur]=gradient(ur,dx,dy);
[Gxvr,Gyvr]=gradient(vr,dx,dy);

% Construct the diffusion operator
% sigmaxx=1-Ghx.*Ghx/(norm([Ghx,Ghx])^2);
% sigmaxy=sigmaxx-1;
% sigmayy=sigmaxx;
 
% axx=sigmaxx.^2+sigmaxy.^2;
% % axy=sigmaxx.*sigmaxy+sigmaxy.*sigmayy;
% ayy=sigmaxy.^2+sigmayy.^2;
% [Gxaxx,~]=gradient(axx,dx,dy);
% [~,Gyayy]=gradient(ayy,dx,dy);

axx=2*dx*dy*sqrt(Gxul.^2+Gyvl.^2+0.5*(Gyul+Gxvl).^2);
ayy=axx;
[Gxaxx,~]=gradient(axx,dx,dy);
[~,Gyayy]=gradient(ayy,dx,dy);

% Standard brownian motion process
if strcmp(whitenoise_flag,'On')
dBtx=sqrt(dt)*randn;
dBty=sqrt(dt)*randn;
sigmadBtx=sigmaxx*dBtx+sigmaxy*dBty;
sigmadBty=sigmaxy*dBtx+sigmayy*dBty;

% Left flux
FL1=uNl.*hl + (hl.*sigmadBtx*flagX+hl.*sigmadBty*flagY)/dt - 0.5*(axx.*Gxhl*flagX + ayy.*Gyhl*flagY + 2*Gxaxx.*hl*flagX + 2*Gyayy.*hl*flagY);
FL2=ul.*uNl.*hl + 0.5*g*(hl.*hl-topol.*topol)*flagX + ul.*(hl.*sigmadBtx*flagX+hl.*sigmadBty*flagY)/dt - 0.5*(axx.*hl.*Gxul*flagX + ayy.*hl.*Gyul*flagY + 2*Gxaxx.*hl.*ul*flagX + 2*Gyayy.*hl.*ul*flagY + axx.*ul.*Gxhl*flagX + ayy.*ul.*Gyhl*flagY);
FL3=vl.*uNl.*hl + 0.5*g*(hl.*hl-topol.*topol)*flagY + vl.*(hl.*sigmadBtx*flagX+hl.*sigmadBty*flagY)/dt - 0.5*(axx.*hl.*Gxvl*flagX + ayy.*hl.*Gyvl*flagY + 2*Gxaxx.*hl.*vl*flagX + 2*Gyayy.*hl.*vl*flagY + axx.*vl.*Gxhl*flagX + ayy.*vl.*Gyhl*flagY);

% Right flux
FR1=uNr.*hr + (hr.*sigmadBtx*flagX+hr.*sigmadBty*flagY)/dt - 0.5*(axx.*Gxhr*flagX + ayy.*Gyhr*flagY + 2*Gxaxx.*hr*flagX + 2*Gyayy.*hr*flagY);
FR2=ur.*uNr.*hr + 0.5*g*(hr.*hr-topor.*topor)*flagX + ur.*(hr.*sigmadBtx*flagX+hr.*sigmadBty*flagY)/dt - 0.5*(axx.*hr.*Gxur*flagX + ayy.*hr.*Gyur*flagY + 2*Gxaxx.*hr.*ur*flagX + 2*Gyayy.*hr.*ur*flagY + axx.*ur.*Gxhr*flagX + ayy.*ur.*Gyhr*flagY);
FR3=vr.*uNr.*hr + 0.5*g*(hr.*hr-topor.*topor)*flagY + vr.*(hr.*sigmadBtx*flagX+hr.*sigmadBty*flagY)/dt - 0.5*(axx.*hr.*Gxvr*flagX + ayy.*hr.*Gyvr*flagY + 2*Gxaxx.*hr.*vr*flagX + 2*Gyayy.*hr.*vr*flagY + axx.*vr.*Gxhr*flagX + ayy.*vr.*Gyhr*flagY);

else
% % Left flux
% FL1=uNl.*hl - 0.5*(axx.*Gxhl*flagX + ayy.*Gyhl*flagY);% + 2*Gxaxx.*hl*flagX + 2*Gyayy.*hl*flagY) ;
% FL2=ul.*uNl.*hl + 0.5*g*(hl.*hl-topol.*topol)*flagX - 0.5*(axx.*hl.*Gxul*flagX + ayy.*hl.*Gyul*flagY);% + 2*Gxaxx.*hl.*ul*flagX + 2*Gyayy.*hl.*ul*flagY + axx.*ul.*Gxhl*flagX + ayy.*ul.*Gyhl*flagY);
% FL3=vl.*uNl.*hl + 0.5*g*(hl.*hl-topol.*topol)*flagY - 0.5*(axx.*hl.*Gxvl*flagX + ayy.*hl.*Gyvl*flagY);% + 2*Gxaxx.*hl.*vl*flagX + 2*Gyayy.*hl.*vl*flagY + axx.*vl.*Gxhl*flagX + ayy.*vl.*Gyhl*flagY);
% 
% % Right flux
% FR1=uNr.*hr - 0.5*(axx.*Gxhr*flagX + ayy.*Gyhr*flagY);% + 2*Gxaxx.*hr*flagX + 2*Gyayy.*hr*flagY);
% FR2=ur.*uNr.*hr + 0.5*g*(hr.*hr-topor.*topor)*flagX - 0.5*(axx.*hr.*Gxur*flagX + ayy.*hr.*Gyur*flagY);% + 2*Gxaxx.*hr.*ur*flagX + 2*Gyayy.*hr.*ur*flagY + axx.*ur.*Gxhr*flagX + ayy.*ur.*Gyhr*flagY);
% FR3=vr.*uNr.*hr + 0.5*g*(hr.*hr-topor.*topor)*flagY - 0.5*(axx.*hr.*Gxvr*flagX + ayy.*hr.*Gyvr*flagY);% + 2*Gxaxx.*hr.*vr*flagX + 2*Gyayy.*hr.*vr*flagY + axx.*vr.*Gxhr*flagX + ayy.*vr.*Gyhr*flagY);     
    
% Left flux
FL1=uNl.*hl - 0.5*(axx.*Gxhl*flagX + ayy.*Gyhl*flagY);% + 2*Gxaxx.*hl*flagX + 2*Gyayy.*hl*flagY) ;
FL2=ul.*uNl.*hl + 0.5*g*(hl.*hl-topol.*topol)*flagX - 0.5*(axx.*hl.*Gxul*flagX + ayy.*hl.*Gyul*flagY + axx.*ul.*Gxhl*flagX + ayy.*ul.*Gyhl*flagY);% + 2*Gxaxx.*hl.*ul*flagX + 2*Gyayy.*hl.*ul*flagY);
FL3=vl.*uNl.*hl + 0.5*g*(hl.*hl-topol.*topol)*flagY - 0.5*(axx.*hl.*Gxvl*flagX + ayy.*hl.*Gyvl*flagY + axx.*vl.*Gxhl*flagX + ayy.*vl.*Gyhl*flagY);% + 2*Gxaxx.*hl.*vl*flagX + 2*Gyayy.*hl.*vl*flagY);

% Right flux
FR1=uNr.*hr - 0.5*(axx.*Gxhr*flagX + ayy.*Gyhr*flagY);% + 2*Gxaxx.*hr*flagX + 2*Gyayy.*hr*flagY);
FR2=ur.*uNr.*hr + 0.5*g*(hr.*hr-topor.*topor)*flagX - 0.5*(axx.*hr.*Gxur*flagX + ayy.*hr.*Gyur*flagY + axx.*ur.*Gxhr*flagX + ayy.*ur.*Gyhr*flagY + 2*Gxaxx.*hr.*ur*flagX + 2*Gyayy.*hr.*ur*flagY);
FR3=vr.*uNr.*hr + 0.5*g*(hr.*hr-topor.*topor)*flagY - 0.5*(axx.*hr.*Gxvr*flagX + ayy.*hr.*Gyvr*flagY + axx.*vr.*Gxhr*flagX + ayy.*vr.*Gyhr*flagY + 2*Gxaxx.*hr.*vr*flagX + 2*Gyayy.*hr.*vr*flagY); 
end
end

% Roe flux
G(:,:,1)=0.5*(FL1+FR1-alpha2-alpha3);
G(:,:,2)=0.5*(FL2+FR2-(R21.*alpha1)-(R22.*alpha2)-(R23.*alpha3));
G(:,:,3)=0.5*(FL3+FR3-(R31.*alpha1)-(R32.*alpha2)-(R33.*alpha3));
end